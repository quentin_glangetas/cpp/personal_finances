#include "aggregationTable.h"

//void AggregationTable::refresh(std::unordered_map<AggregationKey, float>& aggregationMap) const {
void AggregationTable::refresh() const {
    ImGui::Begin("Aggregation");
    static int row_aggregation = 0;
    static int col_aggregation = 0;
    ImGui::PushItemWidth(150);
    ImGui::Combo("r", &row_aggregation, "Yearly\0Monthly\0\0");
    ImGui::SameLine();
    ImGui::PushItemWidth(150);
    ImGui::Combo("c", &col_aggregation, "Category\0Sub Category\0\0");


    if (ImGui::BeginTable("AggregationTable", 3, getTableFlags()))
    {
        //setUpHeaders();
        for (int row : {0, 1, 2, 4}) 
        {
            ImGui::TableNextRow();
            for (int col : {0, 1, 2}) {
                ImGui::TableSetColumnIndex(col);
                ImGui::Text("row=%d, col=%d", row, col);
            }
        }
        ImGui::EndTable();
    }

    ImGui::End();
}


//void TransactionTable::setUpHeaders() const {
//    ImGui::TableSetupColumn("ID", ImGuiTableColumnFlags_DefaultSort);
//    ImGui::TableSetupColumn("Date", ImGuiTableColumnFlags_DefaultSort);
//    ImGui::TableSetupColumn("Description", ImGuiTableColumnFlags_DefaultSort);
//    ImGui::TableSetupColumn("Amount", ImGuiTableColumnFlags_PreferSortDescending);
//    ImGui::TableSetupColumn("Ccy", ImGuiTableColumnFlags_NoSort);
//    ImGui::TableSetupColumn("Category", ImGuiTableColumnFlags_DefaultSort);
//    ImGui::TableSetupColumn("Subcategory", ImGuiTableColumnFlags_DefaultSort);
//    ImGui::TableHeadersRow();
//}