#pragma once

#include <unordered_map>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "../data/dataCoordinator.h"


class AggregationTable {
public:
	//void refresh(std::unordered_map<AggregationKey, float>& aggregationMap) const;
	void refresh() const;
private:
	int cols = 7;

	void setUpHeaders() const;
	static ImGuiTableFlags getTableFlags() {
		return  ImGuiTableFlags_Sortable | ImGuiTableFlags_SortMulti | ImGuiTableFlags_SortTristate;
	}
};