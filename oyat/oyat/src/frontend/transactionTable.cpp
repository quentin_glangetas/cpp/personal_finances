#include "transactionTable.h"
#include <iostream>

void TransactionTable::refresh(const std::vector<Transaction>& transactions) const {
    ImGui::Begin("Transactions");

    // Filtering
    auto transactionFilter = Filter();
    setUpMinDateFilterCombo(transactionFilter);
    ImGui::SameLine();
    setUpCategoryFilterCombo(transactionFilter);
    ImGui::SameLine();
    setUpSubcategoryFilterCombo(transactionFilter);
    ImGui::SameLine();
    setUpSideFilterCombo(transactionFilter);

    // Table definition
    if (ImGui::BeginTable("TransactionTable", cols, getTableFlags()))
    {
        setUpHeaders();
        for (const Transaction& transaction : transactions)
        {
            if (transactionFilter(transaction)) {
                ImGui::TableNextRow();
                refreshTransactionRow(transaction);
            }
        }
        ImGui::EndTable();
    }

    ImGui::End();
}

void TransactionTable::setUpFilterCombo(const std::vector<String>& items, const String& title, int& currentIdx) const
{
    const String& comboPreviewValue = items[currentIdx];  // Pass in the preview value visible before opening the combo (it could be anything)

    const float comboWidth = 175.0f;
    ImGui::SetNextItemWidth(comboWidth);
    if (ImGui::BeginCombo(title.data(), comboPreviewValue.data()))
    {
        for (int n = 0; n < items.size(); n++)
        {
            const bool isSelected = (currentIdx == n);
            if (ImGui::Selectable(items[n].data(), isSelected))
                currentIdx = n;

            // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
            if (isSelected)
                ImGui::SetItemDefaultFocus();
        }
        ImGui::EndCombo();
    }
    
}

inline void TransactionTable::setUpMinDateFilterCombo(Filter& transactionFilter) const {
    static int dateFilterCurrentIdx = 0;
    static const std::vector<String> dateFilterComboOptions = { "All", "Last month", "Last 3 months", "Last year" };
    setUpFilterCombo(dateFilterComboOptions, "##date combo", dateFilterCurrentIdx);

    int month = utils::getCurrentMonth();
    int year = utils::getCurrentYear();
    // TODO: come up with general formulas
    if (dateFilterCurrentIdx == 1) {
        if (month == 1) {
            transactionFilter.setMinDate((year-1) * 10000 + 1201); // first of december of previous year
        }
        else {
            transactionFilter.setMinDate(year * 10000 + (month - 1) * 100 + 01); // first day of last month
        }
    }
    else if (dateFilterCurrentIdx == 2) {
        if (month == 1) {
            int firstOfOctober = 1001;
            transactionFilter.setMinDate((year - 1) * 10000 + firstOfOctober);
        }
        else if (month == 2) {
            int firstOfNovember = 1101;
            transactionFilter.setMinDate((year - 1) * 10000 + firstOfNovember);
        }
        else if (month == 3) {
            int firstOfDecember = 1201;
            transactionFilter.setMinDate((year - 1) * 10000 + firstOfDecember);
        }
        else {
            transactionFilter.setMinDate(year * 10000 + (month - 3) * 100 + 01); // first day of 3 months ago
        }

    }
    else if (dateFilterCurrentIdx == 3) {
        transactionFilter.setMinDate((year-1) * 10000 + 101);
    }
}

inline void TransactionTable::setUpCategoryFilterCombo(Filter& transactionFilter) const
{
    std::vector<String> categoriesSet = dc.getCategories();
    categoriesSet.emplace(categoriesSet.begin(), "All");
    
    static int categoryCurrentIdx = 0;
    setUpFilterCombo(categoriesSet, "##category combo", categoryCurrentIdx);

    if (categoryCurrentIdx)
        transactionFilter.setCategory(categoriesSet[categoryCurrentIdx]);
}

inline void TransactionTable::setUpSubcategoryFilterCombo(Filter& transactionFilter) const
{
    std::vector<String> subcategoriesSet = dc.getSubCategories();
    subcategoriesSet.emplace(subcategoriesSet.begin(), "All");

    static int subcategoryCurrentIdx = 0;
    setUpFilterCombo(subcategoriesSet, "##subcategory combo", subcategoryCurrentIdx);

    if (subcategoryCurrentIdx)
        transactionFilter.setSubcategory(subcategoriesSet[subcategoryCurrentIdx]);
}

inline void TransactionTable::setUpSideFilterCombo(Filter& transactionFilter) const {
    static int sideFilterCurrentIdx = 0;
    static const std::vector<String> sideFilterComboOptions = { "All", "Paid", "Received" };
    setUpFilterCombo(sideFilterComboOptions, "##side combo", sideFilterCurrentIdx);

    if (sideFilterCurrentIdx == 1) {
        transactionFilter.setSide(Transaction::Side::PAY);
    }
    else if (sideFilterCurrentIdx == 2) {
        transactionFilter.setSide(Transaction::Side::RECEIVE);
    }
}

void TransactionTable::setUpHeaders() const {
    ImGui::TableSetupColumn("ID", ImGuiTableColumnFlags_DefaultSort);
    ImGui::TableSetupColumn("Date", ImGuiTableColumnFlags_DefaultSort);
    ImGui::TableSetupColumn("Description", ImGuiTableColumnFlags_DefaultSort);
    ImGui::TableSetupColumn("Amount", ImGuiTableColumnFlags_PreferSortDescending);
    ImGui::TableSetupColumn("Ccy", ImGuiTableColumnFlags_NoSort);
    ImGui::TableSetupColumn("Category", ImGuiTableColumnFlags_DefaultSort);
    ImGui::TableSetupColumn("Subcategory", ImGuiTableColumnFlags_DefaultSort);
    ImGui::TableHeadersRow();
}


void TransactionTable::refreshTransactionRow(const Transaction& transaction) const {
    ImGui::TableNextColumn();
    ImGui::Text("%d", transaction.id);
    ImGui::TableNextColumn();
    std::string prettyDate = constants::shortMonth[transaction.getMonth() % 12];
    prettyDate.append(std::to_string(transaction.getYear() % 100));
    ImGui::Text(prettyDate.data());
    ImGui::TableNextColumn();
    ImGui::Text(transaction.name.data());
    ImGui::TableNextColumn();
    float amount = transaction.side == Transaction::Side::PAY ? transaction.amount : -transaction.amount;
    ImGui::Text("%.2f", amount);
    ImGui::TableNextColumn();
    ImGui::Text(transaction.currency.data());
    ImGui::TableNextColumn();
    ImGui::Text(transaction.category.data());
    ImGui::TableNextColumn();
    ImGui::Text(transaction.subcategory.data());
    ImGui::TableNextColumn();
    String buttonId = "Delete##" + std::to_string(transaction.id);
    if (ImGui::SmallButton(buttonId.data())) {
        dc.deleteTransaction(transaction);
    }
    
}