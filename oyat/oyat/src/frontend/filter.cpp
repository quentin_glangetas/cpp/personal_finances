#include "filter.h"

bool Filter::operator()(const Transaction& transaction) const {
	bool test = true;

	test &= (minDate == -1) || (transaction.date >= minDate);
	test &= (category == "") || (transaction.category == category);
	test &= (subcategory == "") || (transaction.subcategory == subcategory);
	test &= (side == Transaction::Side::UNKNOWN) || (transaction.side == side);

	return test;
}

void Filter::reset() {
	minDate = -1;
	category = "";
	subcategory = "";
	side = Transaction::Side::UNKNOWN;
}