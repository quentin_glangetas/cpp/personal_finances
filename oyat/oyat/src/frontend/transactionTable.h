#pragma once

#include <unordered_set>
#include <vector>
#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include "filter.h"
#include "../data/dataCoordinator.h"
#include "../constants.h"
#include "../utils.h"

class TransactionTable {
public:
	TransactionTable() = default;
	void refresh(const std::vector<Transaction>& transactions) const;


private:
	int cols = 8;
	DataCoordinator& dc = DataCoordinator::getInstance();

	// Filters
	void setUpFilterCombo(const std::vector<String>& items, const String& title, int& currentIdx) const;
	inline void setUpMinDateFilterCombo(Filter& transactionFilter) const;
	inline void setUpCategoryFilterCombo(Filter& transactionFilter) const;
	inline void setUpSubcategoryFilterCombo(Filter& transactionFilter) const;
	inline void setUpSideFilterCombo(Filter& transactionFilter) const;

	// Table
	void setUpHeaders() const;
	void refreshTransactionRow(const Transaction& transaction) const;
	static ImGuiTableFlags getTableFlags() { 
		return  ImGuiTableFlags_Sortable | ImGuiTableFlags_SortMulti | ImGuiTableFlags_SortTristate;
	}
};