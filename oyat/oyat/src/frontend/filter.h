#pragma once
#include <string>
#include "../data/transaction.h"

using String = std::string;

class Filter {
public:
	Filter() = default;
	bool operator()(const Transaction& transaction) const;
	void setMinDate(uint32_t minDate_) { minDate = minDate_; };
	void setCategory(const String& category_) { category = category_; };
	void setSubcategory(const String& subcategory_) { subcategory = subcategory_; };
	void setSide(Transaction::Side side_) { side = side_; };
	void reset();

private:
	uint32_t minDate = -1;
	String category = "";
	String subcategory = "";
	Transaction::Side side = Transaction::Side::UNKNOWN;
};


