#pragma once

#include <memory>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>
#include "databaseConfig.h"
#include "transaction.h"

// TODO: singleton
// TODO: study serialization
class DiskDatabase {

public:
	DiskDatabase();
	void writeAll(const std::vector<Transaction>& transactions) const;
	std::vector<Transaction> retrieveTransactions() const;

private:
	Transaction buildTransaction(const String& line) const;
	String writeTransaction(const Transaction& transaction) const;
	String writeHeaderline() const;
};