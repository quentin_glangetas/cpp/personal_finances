#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <array>

using String = std::string;


struct Transaction {
	enum class Side { PAY, RECEIVE, UNKNOWN };

	uint32_t id;

	uint32_t date = 0;
	String name = "";

	String category = "";
	String subcategory = "";

	float amount = -1;
	String currency = "";
	Side side = Transaction::Side::PAY;

	explicit Transaction(const uint32_t id_) : id(id_) {}

	void setTransactionInfo(const uint32_t date_, const String& name_) {
		date = date_;
		name = name_;
	}

	void setCategoryInfo(const String& category_, const String& subcategory_) {
		category = category_;
		subcategory = subcategory_;
	}

	void setMoneyInfo(const float amount_, const String& currency_, const Transaction::Side side_) {
		amount = amount_;
		currency = currency_;
		side = side_;
	}

	bool operator==(const Transaction& other) const {
		return (id == other.id);
	}

	uint32_t getYear() const {
		return date / 10000;
	}

	uint32_t getMonth() const {
		return (date / 100) % 100;
	}

	// TODO: also include FX conversion to main ccy
	float getNetAmount() const {
		if (side == Side::PAY)
			return amount;
		else if (side == Side::RECEIVE)
			return -amount;
		else
			return 0;
	}

};

