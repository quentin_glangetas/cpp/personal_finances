#include "dataCoordinator.h"


DataCoordinator::DataCoordinator() {
	diskDb = DiskDatabase();
	std::vector<Transaction> transactions = diskDb.retrieveTransactions();
	memoryDb = MemoryDatabase(transactions);
	aggDb = AggregationDatabase(transactions);
}

void DataCoordinator::save() const
{
	save(memoryDb.getTransactions());
}

void DataCoordinator::save(const std::vector<Transaction>& transactions) const {
	diskDb.writeAll(transactions);
}
