#pragma once

#include <string>

using String = std::string;

namespace DatabaseConfig {
	static const String databaseDirectory = "database.csv";
	static const String transactionTableName = "";
	static const int transactionGuessCount = 100;
	static const int databaseColumnCount = 10;

	namespace Queries {
		static const String createDatabaseSql = "";
		static const String createTransactionTableSql = "";
	}
}