#include "diskDatabase.h"




DiskDatabase::DiskDatabase() = default;

std::vector<Transaction> DiskDatabase::retrieveTransactions() const {
	auto file = std::fstream(DatabaseConfig::databaseDirectory, std::ios::in);
	std::vector<Transaction> transactions;
	transactions.reserve(DatabaseConfig::transactionGuessCount);
	String line;

	if (file.is_open()) {
		// header
		getline(file, line);

		// transaction content
		while (getline(file, line)) {
			transactions.push_back(buildTransaction(line));
		}
	}
	else {
		throw R"(Couldn't open database file.)";
	}

	file.close();

	return transactions;
}

void DiskDatabase::writeAll(const std::vector<Transaction>& transactions) const {
	auto file = std::fstream(DatabaseConfig::databaseDirectory, std::ios::trunc | std::ios::out);

	file << writeHeaderline() << std::endl;

	for (auto& transaction : transactions) {
		file << writeTransaction(transaction) << std::endl;
	}

	file.close();
}

Transaction DiskDatabase::buildTransaction(const String& line) const {
	String word;
	std::stringstream transactionStream(line);

	getline(transactionStream, word, ',');
	const auto id = static_cast<uint32_t>(std::stoul(word));
	auto transaction = Transaction(id);

	getline(transactionStream, word, ',');
	const auto date = static_cast<uint32_t>(std::stoul(word));
	getline(transactionStream, word, ',');
	const String name = word; 
	transaction.setTransactionInfo(date, name);

	getline(transactionStream, word, ',');
	const String category = word; 
	getline(transactionStream, word, ',');
	const String subcategory = word; 
	transaction.setCategoryInfo(category, subcategory);

	getline(transactionStream, word, ',');
	const float amount = std::stof(word);
	getline(transactionStream, word, ',');
	const String currency = word;
	getline(transactionStream, word, ',');
	const Transaction::Side side = word == "PAY" ? Transaction::Side::PAY : Transaction::Side::RECEIVE;
	transaction.setMoneyInfo(amount, currency, side);
	
	return transaction;
}

String DiskDatabase::writeTransaction(const Transaction& transaction) const
{
	std::stringstream stringStream;
	String side = transaction.side == Transaction::Side::PAY ? "PAY" : "RECEIVE";
	stringStream
		<< transaction.id << ','
		<< transaction.date << ','
		<< transaction.name << ','
		<< transaction.category << ','
		<< transaction.subcategory << ','
		<< transaction.amount << ','
		<< transaction.currency << ','
		<< side;

	String transactionString = stringStream.str();
	return transactionString;
}

String DiskDatabase::writeHeaderline() const
{
	String headerline = "id,date,name,category,subcategory,amount,currency,side";
	return headerline;
}

