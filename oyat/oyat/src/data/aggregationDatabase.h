#pragma once

#include <vector>
#include <unordered_map>
#include <string>
#include <functional>
#include "transaction.h"

using String = std::string;


struct AggregationKey {
	static const uint32_t ANY_YEAR = 0;
	static const uint32_t ANY_MONTH = 0;
	static const String ANY_CAT;
	static const String ANY_SUBCAT;


	uint32_t year;
	uint32_t month;
	String category;
	String subcategory;

	bool isUniqueKey() const {
		return (year != ANY_YEAR) && (month != ANY_MONTH) && (category != ANY_CAT) && (subcategory != ANY_SUBCAT);
	}

	bool operator==(const AggregationKey& other) const {
		return (year == other.year) && (month == other.month)
			&& (category == other.category) && (subcategory == other.subcategory);
	}

};

template<>
struct std::hash<AggregationKey> {
	auto operator()(const AggregationKey& aggKey) const -> std::size_t {
		auto hash1 = std::hash<uint32_t>{}(aggKey.year) ^ std::hash<uint32_t>{}(aggKey.month);
		auto hash2 = std::hash<std::string>{}(aggKey.category) ^ std::hash<std::string>{}(aggKey.subcategory);
		return hash1 ^ hash2;
	}
};


class AggregationDatabase {
public:

	AggregationDatabase() = default;
	~AggregationDatabase() = default;
	explicit AggregationDatabase(const std::vector<Transaction>& transactions);

	void addTransaction(const Transaction& transaction) {};
	void deleteTransaction(const Transaction& transaction) {};

	float getAmount(const AggregationKey& key) const;
	float getAmount(uint32_t year, uint32_t month, const String& category, const String& subcategory) const;
	std::unordered_map<AggregationKey, float> getAggregationMap() const { return aggregationMap; }


private: 
	std::unordered_map<AggregationKey, float> aggregationMap;
	float getAmountUniqueKey(const AggregationKey& key) const; 
	float getAmountIterKeys(const AggregationKey& key) const;
	bool includeTransactionInIteration(const AggregationKey& realKey, const AggregationKey& filterKey) const;
};

