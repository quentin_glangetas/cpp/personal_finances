#include "aggregationDatabase.h"

const String AggregationKey::ANY_CAT = "any";
const String AggregationKey::ANY_SUBCAT = "any";


AggregationDatabase::AggregationDatabase(const std::vector<Transaction>& transactions)
{
	AggregationKey key;
	for (auto& transaction : transactions) {
		key = { transaction.getYear(), transaction.getMonth(), transaction.category, transaction.subcategory };
		// Need to install C++20 to clean up with unordered_map::contains
		if (aggregationMap.find(key) == aggregationMap.end()) {
			aggregationMap[key] = 0.0f;
		}
		aggregationMap[key] += transaction.getNetAmount();
	}
}

float AggregationDatabase::getAmount(const AggregationKey& key) const {
	return key.isUniqueKey() ? getAmountUniqueKey(key) : getAmountIterKeys(key);
}

float AggregationDatabase::getAmount(uint32_t year, uint32_t month, const String& category, const String& subcategory) const {
	return getAmount({ year, month, category, subcategory });
}

float AggregationDatabase::getAmountUniqueKey(const AggregationKey& key) const {
	if (aggregationMap.find(key) == aggregationMap.end()) {
		return 0.0f;
	}
	else {
		return 0.0f;// aggregationMap[key];
	}
}

float AggregationDatabase::getAmountIterKeys(const AggregationKey& key) const
{
	float result = 0.0f;

	for (const auto& keyAmount : aggregationMap) {
		if (includeTransactionInIteration(keyAmount.first, key)) {
			result += keyAmount.second;
		}
	}

	return result;
}

bool AggregationDatabase::includeTransactionInIteration(const AggregationKey& realKey, const AggregationKey& filterKey) const
{
	bool test = true;

	test &= (filterKey.year == AggregationKey::ANY_YEAR) || (filterKey.year == realKey.year);
	test &= (filterKey.month == AggregationKey::ANY_MONTH) || (filterKey.month == realKey.month);
	test &= (filterKey.category == AggregationKey::ANY_CAT) || (filterKey.category == realKey.category);
	test &= (filterKey.subcategory == AggregationKey::ANY_SUBCAT) || (filterKey.subcategory == realKey.subcategory);

	return test;
}


