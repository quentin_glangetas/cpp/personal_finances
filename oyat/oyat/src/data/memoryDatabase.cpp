#include "memoryDatabase.h"


MemoryDatabase::MemoryDatabase(const std::vector<Transaction>& transactions) : transactions(transactions) {
	for (auto& transaction : transactions) {
		addToVectorIfNotIn(categories, transaction.category);
		addToVectorIfNotIn(subcategories, transaction.subcategory);
		addToVectorIfNotIn(years, transaction.getYear());
		addToVectorIfNotIn(months, transaction.getYear() * 100 + transaction.getMonth());
	}
	std::sort(categories.begin(), categories.end());
	std::sort(subcategories.begin(), subcategories.end());
	std::sort(years.rbegin(), years.rend());
	std::sort(months.rbegin(), months.rend());
}

void MemoryDatabase::addTransaction(const Transaction& transaction) { transactions.push_back(transaction); }

void MemoryDatabase::deleteTransaction(const Transaction& transaction) { 
	transactions.erase(std::remove(transactions.begin(), transactions.end(), transaction), transactions.end());
}

