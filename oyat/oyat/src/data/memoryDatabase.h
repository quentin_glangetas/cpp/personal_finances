#pragma once

#include <algorithm>
#include <vector>
#include "databaseConfig.h"
#include "transaction.h"

class MemoryDatabase {
private:
	std::vector<Transaction> transactions;
	std::vector<String> categories;
	std::vector<String> subcategories;
	std::vector<uint32_t> years;
	std::vector<uint32_t> months;
	template <typename T>
	static void addToVectorIfNotIn(std::vector<T>& v, const T& element) {
		if (std::find(v.begin(), v.end(), element) == v.end())
			v.emplace_back(element);
	}
public:
	explicit MemoryDatabase(const std::vector<Transaction>& transactions);
	MemoryDatabase() = default;
	void addTransaction(const Transaction& transaction);
	void deleteTransaction(const Transaction& transaction);
	const std::vector<Transaction>& getTransactions() const { return transactions; }
	const std::vector<uint32_t>& getUniqueYears() const { return years; }
	const std::vector<uint32_t>& getUniqueMonths() const { return months; }
	const std::vector<String>& getCategories() const { return categories; }
	const std::vector<String>& getSubCategories() const { return subcategories; }
};