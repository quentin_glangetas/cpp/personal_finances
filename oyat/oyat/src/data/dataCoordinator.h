#pragma once

#include <vector>
#include <unordered_map>

#include "diskDatabase.h"
#include "memoryDatabase.h"
#include "aggregationDatabase.h"
#include "transaction.h"


// Singleton Design Pattern
class DataCoordinator {

public:
	static DataCoordinator& getInstance() {
		static DataCoordinator instance;
		return instance;
	}
	~DataCoordinator() = default;

	// include try/catch of below and restore state if error
	void addTransaction(const Transaction& transaction) {
		memoryDb.addTransaction(transaction);
		aggDb.addTransaction(transaction);
	};
	void deleteTransaction(const Transaction& transaction) {
		memoryDb.deleteTransaction(transaction);
		aggDb.deleteTransaction(transaction);
	};
	void save(const std::vector<Transaction>& transactions) const;
	void save() const;

	// iterating passed to memoryDatabase
	const std::vector<Transaction>& getTransactions() const { return memoryDb.getTransactions(); }
	const std::vector<uint32_t>& getUniqueYears() const { return memoryDb.getUniqueYears(); }
	const std::vector<uint32_t>& getUniqueMonths() const { return memoryDb.getUniqueMonths(); }
	const std::vector<String>& getCategories() const { return memoryDb.getCategories(); }
	const std::vector<String>& getSubCategories() const { return memoryDb.getSubCategories(); }

	std::unordered_map<AggregationKey, float> getAggregationMap() const { return aggDb.getAggregationMap(); }

private:
	DataCoordinator();

	DiskDatabase diskDb;
	MemoryDatabase memoryDb;
	AggregationDatabase aggDb;

public:
	DataCoordinator(const DataCoordinator&) = delete;
	void operator=(const DataCoordinator&) = delete;
};