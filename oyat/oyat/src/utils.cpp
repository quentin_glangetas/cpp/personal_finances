#include "utils.h"


namespace utils {
    int getCurrentMonth()
    {
        struct tm newtime;
        time_t now = time(0);
        localtime_s(&newtime, &now);
        int month = 1 + newtime.tm_mon;
        return month;
    }

    int getCurrentYear()
    {
        struct tm newtime;
        time_t now = time(0);
        localtime_s(&newtime, &now);
        int year = newtime.tm_year + 1900;
        return year;
    }
}