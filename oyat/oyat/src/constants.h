#pragma once
#include <array>
#include <string>


namespace constants {
	static const std::array<std::string, 13> shortMonth = { 
		"Dec", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sep", "Oct", "Nov", "Dec" 
	}; // If modulo 12, index 0 is Dec
}
