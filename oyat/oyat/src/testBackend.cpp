#include <iostream>
#include "data/aggregationDatabase.h"
#include "data/dataCoordinator.h"
#include <unordered_map>
#include <functional>
#include <string>
#include "constants.h"
#include <ctime>



int getMonth()
{
	struct tm newtime;
	time_t now = time(0);
	localtime_s(&newtime, &now);
	int Month = 1 + newtime.tm_mon;
	return Month;
}

int backMain() {

	std::cout << getMonth() << std::endl;

	return 0;

}