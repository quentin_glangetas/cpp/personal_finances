#include <string>
#include <iostream>

int frontMain();
int backMain();


int main() {
	std::string s;
	std::cin >> s;
	static const bool doBack = s == "0";

	if (doBack) {
		return backMain();
	}
	else {
		return frontMain();
	}


	return 0;
}