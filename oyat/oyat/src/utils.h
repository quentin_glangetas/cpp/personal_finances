#pragma once
#include <ctime>

namespace utils {
    int getCurrentMonth();
    int getCurrentYear();

}
